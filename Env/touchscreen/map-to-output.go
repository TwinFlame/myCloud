# Wiki  https://wiki.archlinux.org/index.php/touchscreen#Using_a_touchscreen_in_a_multi-head_setup
# discuss   https://askubuntu.com/questions/51445/how-do-i-calibrate-a-touchscreen-on-a-dual-monitor-system

check(){
    xinput
    xrandr
}

myTouchscreen(){
    xinput map-to-output 14 HDMI-1-4
    xinput map-to-output 15 HDMI-2-5
}